### Notice ###

This project uses both C# and Python. 

Due to the use of .NET Framework, this project can only run on 
Microsoft Windows.

The Python scripts are technically multiplatform and can be run
separately, but this would lose all the advantages that the 
graphical user interface offers.

### Datasets ###

The datasets are included in the SourceData folder,
named after the dataset from which it was created.

The processed datasets are included in the ProcessedData folder
and can be loaded individually.


### Compile instruction (Windows only) ###

1. Install Python 3.7 or newer, make sure python.exe is 
   accessible through the PATH environment.

2. Install Visual Studio 2017 and .NET Framework 4.7.2 or newer.

3. Open the solution file MultimediaRetrievala.sln in Visual 
   Studio 2017 or newer.

4. Restore the NuGet packages. (Right click on the solution in 
   the Solution Explorer and select “Restore NuGet Packages”)

5. Run the project using Debug, Start Debugging or press F5.

The application is started successfully when a console window 
opens. The window will first display the recognized version of 
Python. It should then create a virtual environment for Python 
and install all the needed packages. This takes a while, but 
needs to only be done once. Once all the packages have been 
installed, the main window will open.

### Running t-SNE ###

1. Place the extract_tsne_data.py script in a directory containing 
   "vectors.json" to generate data.txt which can be used for t-SNE input.
   Run it to generate data.txt

2. Place the extract_tsne_labels.py script in a directory containing 
   the dataset directories(the directories with hashes as names) 
   to generate data.txt which can be used for t-SNE input.
   Run it to generate labels.txt
   
3. Make sure the following Python libraries are installed before running 
   calculate_tsne.py:
	- mplcursors
	- numpy
	- sklearn
	- matplotlib, where version >= 3.1
	
4. Run calculate_tsne.py using the command line:
	python calculate_tsne.py <path_to_data.txt> <path_to_labels.txt>
