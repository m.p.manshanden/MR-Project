﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace MultimediaRetrieval
{
    internal static class Python
    {
        private static readonly string VirtualEnvLocation = "../Python/";
        private static readonly string[] RequiredPackages = new[] { "pyopengl", "glfw", "pyglm", "open3d", "numpy", "scipy", "pillow", "opencv-python", "scikit-image", "annoy" };

        public static void Load()
        {
            string pythonInstance = RunExecutable("python", "--version");

            if (!pythonInstance.StartsWith("Python"))
            {
                throw new Exception("Could not find a python instance.");
            }

            Console.WriteLine("Using " + pythonInstance);

            CreateVirtualEnv(VirtualEnvLocation);
            string[] installed = GetInstalledPackages();
            IEnumerable<string> packages = RequiredPackages.Except(installed);
            InstallPackages(packages);

            Console.WriteLine("Python virtualenv initialized successfully");
        }

        private static string RunExecutable(string executable, string args, bool showOutput = false)
        {
            var start = new ProcessStartInfo
            {
                FileName = executable + ".exe",
                Arguments = args,
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                UseShellExecute = false,
            };

            string output = "";

            using (var process = Process.Start(start))
            {
                string line = process.StandardOutput.ReadLine();

                while (line != null)
                {
                    output += line + "\n";

                    if (showOutput)
                    {
                        Console.WriteLine(line);
                    }

                    line = process.StandardOutput.ReadLine();
                }

                process.WaitForExit();
                return output.Trim();
            }
        }

        private static void CreateVirtualEnv(string directory)
        {
            Console.WriteLine("Initializing python virtualenv...");

            if (Directory.Exists(directory))
            {
                return;
            }

            string output = RunExecutable("python", $"-m venv {directory}");

            if (!Directory.Exists(directory))
            {
                throw new Exception("Could not create virtualenv. Output message: " + output);
            }
        }

        private static void InstallPackages(IEnumerable<string> packages)
        {
            foreach (string package in packages)
            {
                Console.WriteLine($"Installing package {package}...");
                RunExecutable(VirtualEnvLocation + "Scripts/pip", $"install {package}");
            }
        }

        private static string[] GetInstalledPackages()
        {
            var packages = new List<string>();
            string output = RunExecutable(VirtualEnvLocation + "Scripts/pip", "list");
            string[] lines = output.Split('\n');

            for (int i = 2; i < lines.Length; i++)
            {
                string package = lines[i].Split(' ')[0].ToLower();

                if (package != String.Empty)
                {
                    packages.Add(package);
                }
            }

            return packages.ToArray();
        }

        public static Process Run(string script, params string[] arguments)
        {
            var start = new ProcessStartInfo
            {
                FileName = VirtualEnvLocation + "Scripts/python",
                Arguments = $"{script} {ToArgsString(arguments)}",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            return Process.Start(start);
        }

        public static Process RunAsync(string script, params string[] arguments)
        {
            var start = new ProcessStartInfo
            {
                FileName = VirtualEnvLocation + "Scripts/python",
                Arguments = $"{script} {ToArgsString(arguments)}",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                CreateNoWindow = true,
                UseShellExecute = false
            };

            var process = Process.Start(start);

            Console.WriteLine($"Started process with PID {process.Id}");

            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            process.ErrorDataReceived += (sender, e) => Console.WriteLine(e.Data);

            return process;
        }

        public static void KillTree(this Process process)
        {
            Process.Start(new ProcessStartInfo
            {
                FileName = "taskkill",
                Arguments = $"/PID {process.Id} /F /T",
                CreateNoWindow = true,
                UseShellExecute = false
            }).WaitForExit();

            Console.WriteLine($"Killed process with PID {process.Id}");
        }

        private static string ToArgsString(string[] arguments)
        {
            string args = "";

            for (int i = 0; i < arguments.Length; i++)
            {
                arguments[i] = "\"" + arguments[i] + "\"";
            }

            if (arguments.Length > 0)
            {
                args = arguments.Aggregate((a, b) => a + " " + b);
            }

            return args;
        }
    }
}
