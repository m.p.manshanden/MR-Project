﻿using OpenTK.Graphics.OpenGL4;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MultimediaRetrieval.Graphics
{
    internal class VertexBuffer
    {
        public int Handle { get; }
        public int Count { get; private set; }

        private readonly List<int> attribIndices;
        private readonly List<int> attribSizes;
        private int stride;

        public VertexBuffer()
        {
            this.Handle = GL.GenBuffer();

            this.attribIndices = new List<int>();
            this.attribSizes = new List<int>();
        }

        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, this.Handle);

            int offset = 0;

            for (int i = 0; i < this.attribIndices.Count; i++)
            {
                GL.VertexAttribPointer(this.attribIndices[i], this.attribSizes[i], VertexAttribPointerType.Float, false, this.stride, offset);
                GL.EnableVertexAttribArray(this.attribIndices[i]);

                offset += this.attribSizes[i];
            }
        }

        public void AddFloatAttribute(ShaderProgram program, string name, int size)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, this.Handle);
            int location = GL.GetAttribLocation(program.Handle, name);

            this.AddFloatAttribute(location, size);
        }

        public void AddFloatAttribute(int location, int size)
        {
            this.attribIndices.Add(location);
            this.attribSizes.Add(size);
            this.stride += size * 4;
        }

        public void BufferData<T>(T[] data, int count)
            where T : struct
        {
            int elemSize = Marshal.SizeOf<T>();

            GL.BindBuffer(BufferTarget.ArrayBuffer, this.Handle);
            GL.BufferData(BufferTarget.ArrayBuffer, elemSize * count, data, BufferUsageHint.StaticDraw);

            this.Count = count;
        }

        public void BufferData<T>(T[] data)
            where T : struct
        {
            this.BufferData(data, data.Length);
        }
    }
}
