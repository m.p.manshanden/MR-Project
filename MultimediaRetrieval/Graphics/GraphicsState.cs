﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.Drawing;

namespace MultimediaRetrieval.Graphics
{
    internal static class GraphicsState
    {
        public static void GetError()
        {
            ErrorCode error = GL.GetError();

            while (error != ErrorCode.NoError)
            {
                Console.WriteLine("OpenGL Error: " + error);
                error = GL.GetError();
            }
        }

        public static void ClearBackbuffer()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }

        public static void ClearColor(Color color)
        {
            GL.ClearColor(color);
        }

        public static void ClearColor(float r, float g, float b)
        {
            GL.ClearColor(r, g, b, 1);
        }

        public static void ClearColor(Vector3 color)
        {
            ClearColor(color.X, color.Y, color.Z);
        }

        public static void Viewport(int width, int height)
        {
            GL.Viewport(0, 0, width, height);
        }

        public static void EnableDepthTest()
        {
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);
        }

        public static void DisableDepthTest()
        {
            GL.Disable(EnableCap.DepthTest);
        }

        public static void CullNone()
        {
            GL.Disable(EnableCap.CullFace);
        }

        public static void EnableWireframe()
        {
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
        }

        public static void DisableWireFrame()
        {
            GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
        }

        public static void EnableLineSmoothing()
        {
            GL.Enable(EnableCap.LineSmooth);
            GL.Hint(HintTarget.LineSmoothHint, HintMode.Nicest);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
        }
    }
}
