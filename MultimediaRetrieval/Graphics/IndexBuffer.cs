﻿using OpenTK.Graphics.OpenGL4;

namespace MultimediaRetrieval.Graphics
{
    internal class IndexBuffer
    {
        public int Handle { get; }
        public int Count { get; private set; }

        public IndexBuffer()
        {
            this.Handle = GL.GenBuffer();
        }

        public void Bind()
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, this.Handle);
        }

        public void BufferData(int[] data, int count)
        {
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, this.Handle);
            GL.BufferData(BufferTarget.ElementArrayBuffer, count * 4, data, BufferUsageHint.StaticDraw);

            this.Count = count;
        }

        public void BufferData(int[] data)
        {
            this.BufferData(data, data.Length);
        }
    }
}
