﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System;
using System.IO;

namespace MultimediaRetrieval.Graphics
{
    internal class ShaderProgram
    {
        public int Handle;

        public ShaderProgram()
        {
            this.Handle = GL.CreateProgram();
        }

        public void Compile(string vertexCode, string fragmentCode)
        {
            int vertexShader = GL.CreateShader(ShaderType.VertexShader);
            int fragmentShader = GL.CreateShader(ShaderType.FragmentShader);

            GL.ShaderSource(vertexShader, vertexCode);
            GL.ShaderSource(fragmentShader, fragmentCode);

            GL.CompileShader(vertexShader);
            GL.GetShader(vertexShader, ShaderParameter.CompileStatus, out int shaderStatus);

            if (shaderStatus == 0)
            {
                string info = GL.GetShaderInfoLog(vertexShader);
                Console.WriteLine(info);
                return;
            }

            GL.AttachShader(this.Handle, vertexShader);

            GL.CompileShader(fragmentShader);
            GL.GetShader(fragmentShader, ShaderParameter.CompileStatus, out shaderStatus);

            if (shaderStatus == 0)
            {
                string info = GL.GetShaderInfoLog(vertexShader);
                Console.WriteLine(info);
                return;
            }

            GL.AttachShader(this.Handle, fragmentShader);

            GL.LinkProgram(this.Handle);
            GL.GetProgram(this.Handle, GetProgramParameterName.LinkStatus, out int linkStatus);

            if (linkStatus == 0)
            {
                string info = GL.GetProgramInfoLog(this.Handle);
                Console.WriteLine(info);
                return;
            }

            GL.DeleteShader(vertexShader);
            GL.DeleteShader(fragmentShader);
        }

        public void CompileFiles(string vertexShaderPath, string fragmentShaderPath)
        {
            string vertexCode = File.ReadAllText(vertexShaderPath);
            string fragmentCode = File.ReadAllText(fragmentShaderPath);

            this.Compile(vertexCode, fragmentCode);
        }


        public void Use()
        {
            GL.UseProgram(this.Handle);
        }

        public void SetUniform(string location, Vector3 value)
        {
            GL.Uniform3(GL.GetUniformLocation(this.Handle, location), value);
        }

        public void SetUniform(string location, float value)
        {
            GL.Uniform1(GL.GetUniformLocation(this.Handle, location), value);
        }

        public void SetUniform(string location, Matrix4 value)
        {
            Matrix4 copy = value;
            GL.UniformMatrix4(GL.GetUniformLocation(this.Handle, location), false, ref copy);
        }
    }
}
