﻿using OpenTK.Graphics.OpenGL4;
using System.Collections.Generic;

namespace MultimediaRetrieval.Graphics
{
    internal class VertexArray
    {
        public ShaderProgram Program { get; }
        public int Handle { get; private set; }

        private readonly List<VertexBuffer> vbos;
        private IndexBuffer ebo;

        public VertexArray()
        {
            this.Handle = GL.GenVertexArray();

            this.vbos = new List<VertexBuffer>();
        }

        private void Reset()
        {
            GL.DeleteVertexArray(this.Handle);
            this.Handle = GL.GenVertexArray();

            GL.BindVertexArray(this.Handle);

            for (int i = 0; i < this.vbos.Count; i++)
            {
                this.vbos[i].Bind();
            }

            if (this.ebo != null)
            {
                this.ebo.Bind();
            }

            GL.BindVertexArray(0);
        }

        public void Bind()
        {
            GL.BindVertexArray(this.Handle);
            GraphicsState.GetError();
        }

        public void AddVbo(VertexBuffer vbo)
        {
            this.vbos.Add(vbo);
            this.Reset();
        }

        public void SetEbo(IndexBuffer ebo)
        {
            this.ebo = ebo;
            this.Reset();
        }

        public void Draw()
        {
            this.Bind();
            GL.DrawElements(PrimitiveType.Triangles, this.ebo.Count, DrawElementsType.UnsignedInt, 0);

            GraphicsState.GetError();
        }
    }
}
