﻿using OpenTK;
using System;

namespace MultimediaRetrieval.Graphics
{
    internal class Camera
    {
        private float distance, theta, phi;
        private int width, height;
        private Vector3 lookat;

        public float Distance
        {
            get => this.distance;
            set
            {
                this.distance = MathHelper.Clamp(value, 0.1f, 20f);
                this.UpdateViewMatrix();
            }
        }
        public float Theta
        {
            get => this.theta;
            set
            {
                this.theta = MathHelper.Clamp(value, 0.001f, MathHelper.Pi - 0.001f);
                this.UpdateViewMatrix();
            }
        }
        public float Phi
        {
            get => this.phi;
            set
            {
                this.phi = value;
                this.UpdateViewMatrix();
            }
        }
        public Vector3 LookAt
        {
            get => this.lookat;
            set
            {
                this.lookat = value;
                this.UpdateViewMatrix();
            }
        }
        public Matrix4 Projection { get; private set; }
        public Matrix4 View { get; private set; }

        public Camera(int screenWidth, int screenHeight)
        {
            this.distance = 3;
            this.theta = MathHelper.PiOver2;
            this.phi = 0;
            this.width = screenWidth;
            this.height = screenHeight;

            this.UpdateProjectionMatrix();
            this.UpdateViewMatrix();
        }

        private void UpdateViewMatrix()
        {
            Vector3 position = ToCartesian(this.distance, this.phi, this.theta) + this.LookAt;
            this.View = Matrix4.LookAt(position, this.LookAt, Vector3.UnitY);
        }

        private void UpdateProjectionMatrix()
        {
            float ar = this.width / (float)this.height;
            this.Projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, ar, 0.01f, 1000.0f);
        }

        public void Resize(int width, int height)
        {
            if (width == 0 || height == 0)
            {
                return;
            }

            this.width = width;
            this.height = height;
            this.UpdateProjectionMatrix();
        }

        private static Vector3 ToCartesian(float r, float phi, float theta)
        {
            return new Vector3(
                r * (float)Math.Sin(theta) * (float)Math.Cos(phi),
                r * (float)Math.Cos(theta),
                r * (float)Math.Sin(theta) * (float)Math.Sin(phi)
            );
        }
    }
}
