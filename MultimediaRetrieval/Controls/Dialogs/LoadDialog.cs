﻿using Microsoft.WindowsAPICodePack.Taskbar;
using System;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls.Dialogs
{
    public partial class LoadDialog : Form
    {
        protected LoadDialog()
        {
            this.InitializeComponent();
            this.DialogResult = DialogResult.Cancel;
        }

        protected void UpdateLabelExt(string data)
        {
            if (this.Disposing || this.IsDisposed)
            {
                return;
            }

            Action action = () => this.label1.Text = data;
            this.Invoke(action, null);
        }

        protected void UpdateTitleExt(string title)
        {
            if (this.Disposing || this.IsDisposed)
            {
                return;
            }

            Action action = () => this.Text = title;
            this.Invoke(action);
        }

        protected void UpdateProgressBarExt(int value, int max)
        {
            if (this.Disposing || this.IsDisposed)
            {
                return;
            }

            Action action = () =>
            {
                this.progressBar1.Style = ProgressBarStyle.Continuous;
                this.progressBar1.Maximum = max;
                this.progressBar1.Value = value;

                TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.Normal, this.Handle);
                TaskbarManager.Instance.SetProgressValue(value, max, this.Handle);
            };

            this.Invoke(action);
        }

        protected void CloseExt(DialogResult dialogResult)
        {
            Action action = () =>
            {
                this.DialogResult = dialogResult;
                this.Close();
            };

            this.Invoke(action);
        }

        public virtual void Cancel()
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Cancel();
            this.button1.Enabled = false;
        }
    }
}
