﻿using OpenTK;
using System.Drawing;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls.Dialogs
{
    public partial class RenderSettingsDialog : Form
    {
        private readonly Renderer renderer;

        public RenderSettingsDialog(Renderer renderer)
        {
            this.InitializeComponent();

            this.renderer = renderer;

            this.checkBox1.Checked = renderer.DrawSolid;
            this.checkBox2.Checked = renderer.DrawWire;
        }

        private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
        {
            this.renderer.DrawSolid = this.checkBox1.Checked;
            this.renderer.Invalidate();
        }

        private void checkBox2_CheckedChanged(object sender, System.EventArgs e)
        {
            this.renderer.DrawWire = this.checkBox2.Checked;
            this.renderer.Invalidate();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            this.renderer.SolidColor = this.OpenColorDialog(this.renderer.SolidColor);
            this.renderer.Invalidate();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            this.renderer.WireColor = this.OpenColorDialog(this.renderer.WireColor);
            this.renderer.Invalidate();
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            this.renderer.ClearColor = this.OpenColorDialog(this.renderer.ClearColor);
            this.renderer.Invalidate();
        }

        private Vector3 OpenColorDialog(Vector3 defaultColor)
        {
            int r = (int)(defaultColor.X * 255);
            int g = (int)(defaultColor.Y * 255);
            int b = (int)(defaultColor.Z * 255);

            var colorDialog = new ColorDialog()
            {
                AnyColor = true,
                SolidColorOnly = true,
                Color = Color.FromArgb(r, g, b)
            };

            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                return new Vector3(colorDialog.Color.R / 255.0f, colorDialog.Color.G / 255.0f, colorDialog.Color.B / 255.0f);
            }

            return defaultColor;
        }

        private void button4_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void button5_Click(object sender, System.EventArgs e)
        {
            this.renderer.ResetToDefault();

            this.checkBox1.Checked = this.renderer.DrawSolid;
            this.checkBox2.Checked = this.renderer.DrawWire;
        }
    }
}
