﻿using System.Diagnostics;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls.Dialogs
{
    public class LoadDatasetDialog : LoadDialog
    {
        private readonly string datasetLocation;
        private readonly Process process;

        private int found;
        private int extraced;

        public Dataset Dataset { get; private set; }

        public LoadDatasetDialog(string datasetLocation)
        {
            this.datasetLocation = datasetLocation;
            this.process = Python.RunAsync("Scripts/Python/query_dataset.py", datasetLocation);

            this.process.OutputDataReceived += this.Process_OutputDataReceived;

            this.Load += (sender, e) =>
            {
                this.UpdateTitleExt("Loading dataset...");
                this.UpdateLabelExt("Loading dataset...");
            };
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null)
            {
                this.CloseExt(this.found == this.extraced ? DialogResult.OK : DialogResult.Cancel);
                return;
            }

            if (e.Data == "Loaded")
            {
                this.process.OutputDataReceived -= this.Process_OutputDataReceived;

                this.Dataset = Dataset.LoadFromDirectory(this.process, this.datasetLocation);
                this.CloseExt(DialogResult.OK);
            }

            if (e.Data.StartsWith("Found"))
            {
                this.found++;
                this.UpdateLabelExt(e.Data);
                this.UpdateTitleExt("Scanning...");
            }

            if (e.Data.StartsWith("Extracted"))
            {
                this.extraced++;
                this.UpdateLabelExt(e.Data);
                this.UpdateTitleExt("Extracting...");
                this.UpdateProgressBarExt(this.extraced, this.found);
            }
        }

        public override void Cancel()
        {
            this.process.KillTree();
        }
    }
}
