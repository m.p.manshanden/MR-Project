﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls.Dialogs
{
    public partial class MeasureResultsDialog : Form
    {
        public MeasureResultsDialog(List<string> results1, List<string> results2, List<string> results3)
        {
            this.InitializeComponent();

            this.textBox1.Lines = results1.ToArray();
            this.textBox2.Lines = results2.ToArray();
            this.textBox3.Lines = results3.ToArray();
        }
    }
}
