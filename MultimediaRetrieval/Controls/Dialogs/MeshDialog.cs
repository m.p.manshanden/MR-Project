﻿using System;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls.Dialogs
{
    public partial class MeshDialog : Form
    {
        private readonly MeshOverview view;

        public DataItem Selected => this.view.SelectedMesh;

        public MeshDialog(Dataset dataSet)
        {
            this.InitializeComponent();

            this.view = new MeshOverview(dataSet)
            {
                Dock = DockStyle.Fill
            };

            this.Controls.Add(this.view);
            this.DialogResult = DialogResult.None;
            this.view.MeshClicked += this.View_MeshClicked;
        }

        private void View_MeshClicked(object sender, EventArgs e)
        {
            this.view.MeshClicked -= this.View_MeshClicked;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
