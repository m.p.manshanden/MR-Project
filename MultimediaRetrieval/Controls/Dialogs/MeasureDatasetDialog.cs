﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls.Dialogs
{
    using StringList = List<string>;

    public class MeasureDatasetDialog : LoadDialog
    {
        private readonly Dataset dataset;
        private readonly Dictionary<string, StringList> classes;

        private bool running;

        public int K { get; }

        public List<string> Results1 { get; }
        public List<string> Results2 { get; }
        public List<string> Results3 { get; }

        public MeasureDatasetDialog(Dataset dataset, int k = 10)
        {
            this.dataset = dataset;
            this.classes = new Dictionary<string, StringList>();

            this.K = k;

            this.Results1 = new List<string>();
            this.Results2 = new List<string>();
            this.Results3 = new List<string>();

            this.Load += (sender, e) =>
            {
                this.UpdateTitleExt("Measuring dataset...");
                this.UpdateLabelExt("Collecting classes...");
            };

            this.Shown += this.MeasureDatasetDialog_Shown;

            FormClosing += (sender, e) => this.running = false;
        }

        private void MeasureDatasetDialog_Shown(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                this.running = true;

                this.Collect();
                this.Measure();

                if (this.running)
                {
                    this.CloseExt(DialogResult.OK);
                }
                else
                {
                    this.CloseExt(DialogResult.Cancel);
                }
            });
        }

        private void Collect()
        {
            foreach (DataItem item in this.dataset)
            {
                string[] labelTree = item.Label.Split('/');
                string label = labelTree[labelTree.Length - 1].ToLower();

                if (!this.classes.ContainsKey(label))
                {
                    this.classes.Add(label, new StringList());
                }

                this.classes[label].Add(item.Mesh);
            }
        }

        private void Measure()
        {
            this.UpdateLabelExt("Measuring classes...");

            int progress = 0;
            float total = 0;
            float correct = 0;

            var results = new List<float>();

            foreach (KeyValuePair<string, StringList> kvp in this.classes)
            {
                this.UpdateLabelExt($"Measuring {kvp.Key}...");
                this.UpdateProgressBarExt(progress, this.classes.Count - 1);

                int classTotal = this.K;// kvp.Value.Count;
                string classLabel = kvp.Key;

                float correctCount = 0;

                foreach (string mesh in kvp.Value)
                {
                    if (!this.running)
                    {
                        return;
                    }

                    Dataset queryResult = this.dataset.Query(mesh, classTotal);

                    foreach (DataItem result in queryResult)
                    {
                        string label = result.Label.ToLower().Split('/').Last();

                        this.Results2.Add(kvp.Key);
                        this.Results3.Add(label);

                        correctCount += label.Contains(kvp.Key) ? 1 : 0;
                    }
                }

                float accuracy = correctCount / (classTotal * kvp.Value.Count);

                results.Add(accuracy);

                this.Results1.Add(kvp.Key + "," + accuracy);

                total += classTotal * kvp.Value.Count;
                correct += correctCount;

                progress += 1;
            }

            this.Results1.Add("Avg," + this.ComputeAverage(results));

            // copy of results without lowest 2 labels.
            IEnumerable<float> results2 = results.OrderBy(s => s).Skip(2);

            this.Results1.Add("Avg2," + this.ComputeAverage(results2));
        }

        public override void Cancel()
        {
            this.running = false;
        }

        private float ComputeAverage(IEnumerable<float> data)
        {
            int count = 0;
            float sum = 0;

            foreach (float date in data)
            {
                sum += date;
                count++;
            }

            return sum / count;
        }
    }
}
