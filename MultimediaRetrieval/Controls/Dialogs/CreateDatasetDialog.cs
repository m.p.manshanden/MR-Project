﻿using System.Diagnostics;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls.Dialogs
{
    public class CreateDatasetDialog : LoadDialog
    {
        private readonly Process process;

        private int found;
        private int processed;

        public CreateDatasetDialog(string source, string target)
        {
            this.process = Python.RunAsync("Scripts/Python/process_dataset.py", source, target);

            this.process.OutputDataReceived += this.Process_OutputDataReceived;

            this.Load += (sender, e) =>
            {
                this.UpdateTitleExt("Creating dataset...");
                this.UpdateLabelExt("Creating dataset...");
            };
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (e.Data == null)
            {
                this.CloseExt(this.found == this.processed ? DialogResult.OK : DialogResult.Cancel);
                return;
            }

            if (e.Data.StartsWith("Found"))
            {
                this.found++;
                this.UpdateLabelExt(e.Data);
                this.UpdateTitleExt("Scanning...");
            }
            else if (e.Data.StartsWith("Processed"))
            {
                this.processed++;
                this.UpdateLabelExt(e.Data);
                this.UpdateTitleExt("Processing...");
                this.UpdateProgressBarExt(this.processed, this.found);
            }
        }

        public override void Cancel()
        {
            this.process.KillTree();
        }
    }
}
