﻿using MultimediaRetrieval.Geometry;
using MultimediaRetrieval.Graphics;
using OpenTK;
using OpenTK.Graphics;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls
{
    public class Renderer : GLControl
    {
        private ShaderProgram programSolid;
        private ShaderProgram programWire;

        private VertexBuffer positions;
        private VertexBuffer normals;
        private IndexBuffer indices;

        private VertexArray vaoSolid;
        private VertexArray vaoWire;

        private readonly Camera camera;
        private Point mouse;

        public bool DrawWire { get; set; }
        public bool DrawSolid { get; set; }
        public Vector3 WireColor { get; set; }
        public Vector3 SolidColor { get; set; }
        public Vector3 ClearColor { get; set; }
        public Vector3 LightDirection { get; set; }

        public Mesh Mesh { get; private set; }

        public Renderer()
            : base(new GraphicsMode(ColorFormat.Empty, 24, 0, 4), 3, 3, GraphicsContextFlags.ForwardCompatible)
        {
            this.Paint += (sender, e) => this.Render();
            this.Resize += (sender, e) => this.Resized();

            this.camera = new Camera(this.Width, this.Height);
            this.mouse = MousePosition;

            this.Initialize();
            this.ResetToDefault();
        }

        public void ResetToDefault(object sender, EventArgs e)
        {
            this.ResetToDefault();
        }

        public void ResetToDefault()
        {
            this.DrawSolid = this.DrawWire = true;

            this.WireColor = new Vector3(0.0f);
            this.SolidColor = new Vector3(0.9f);
            this.ClearColor = new Vector3(0.1f);
            this.LightDirection = new Vector3(1, 2, 3).Normalized();

            this.camera.Phi = 0;
            this.camera.Theta = 0;

            this.Invalidate();
        }

        private void Initialize()
        {
            this.MakeCurrent();

            this.programSolid = new ShaderProgram();
            this.programSolid.CompileFiles("Static/VertexSolid.glsl", "Static/FragmentSolid.glsl");

            this.programWire = new ShaderProgram();
            this.programWire.CompileFiles("Static/VertexWire.glsl", "Static/FragmentWire.glsl");

            this.positions = new VertexBuffer();
            this.positions.AddFloatAttribute(0, 3);

            this.normals = new VertexBuffer();
            this.normals.AddFloatAttribute(1, 3);

            this.indices = new IndexBuffer();

            this.vaoSolid = new VertexArray();
            this.vaoSolid.AddVbo(this.positions);
            this.vaoSolid.AddVbo(this.normals);
            this.vaoSolid.SetEbo(this.indices);

            this.vaoWire = new VertexArray();
            this.vaoWire.AddVbo(this.positions);
            this.vaoWire.SetEbo(this.indices);

            GraphicsState.ClearColor(0.1f, 0.1f, 0.1f);
            GraphicsState.EnableDepthTest();
            GraphicsState.CullNone();
            GraphicsState.EnableLineSmoothing();

            this.MouseWheel += (sender, e) => this.UpdateCamera(e.Delta);
            this.MouseMove += (sender, e) => this.UpdateCamera(0);
        }

        private void Resized()
        {
            this.camera.Resize(this.Width, this.Height);
            GraphicsState.Viewport(this.Width, this.Height);
            this.Invalidate();
        }

        public void LoadMesh(Mesh mesh)
        {
            this.positions.BufferData(mesh.Vertices);
            this.normals.BufferData(mesh.Normals);
            this.indices.BufferData(mesh.Triangles);

            this.camera.LookAt = mesh.Center;
            this.Mesh = mesh;

            this.Invalidate();
        }

        private void UpdateCamera(int wheelDelta)
        {
            if (Control.MouseButtons == MouseButtons.Left)
            {
                int dx = this.mouse.X - MousePosition.X;
                int dy = this.mouse.Y - MousePosition.Y;

                this.camera.Phi -= dx * 0.01f;
                this.camera.Theta += dy * 0.01f;

                this.Invalidate();
            }

            if (wheelDelta != 0)
            {
                this.camera.Distance += wheelDelta * 0.001f;

                this.Invalidate();
            }

            this.mouse = MousePosition;
        }

        public void Render()
        {
            this.MakeCurrent();

            GraphicsState.ClearColor(this.ClearColor);
            GraphicsState.ClearBackbuffer();

            if (this.DrawSolid)
            {
                GraphicsState.DisableWireFrame();

                this.programSolid.Use();
                this.programSolid.SetUniform("Projection", this.camera.Projection);
                this.programSolid.SetUniform("View", this.camera.View);
                this.programSolid.SetUniform("light_Color", this.SolidColor);
                this.programSolid.SetUniform("light_Direction", this.LightDirection);
                this.programSolid.SetUniform("light_MinIntensity", 0.3f);

                this.vaoSolid.Draw();
            }

            if (this.DrawWire)
            {
                GraphicsState.EnableWireframe();

                this.programWire.Use();
                this.programWire.SetUniform("Projection", this.camera.Projection);
                this.programWire.SetUniform("View", this.camera.View);
                this.programWire.SetUniform("light_Color", this.WireColor);

                this.vaoWire.Draw();
            }

            this.SwapBuffers();
        }
    }
}
