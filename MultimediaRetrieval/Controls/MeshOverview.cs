﻿using OpenTK;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MultimediaRetrieval.Controls
{
    public partial class MeshOverview : UserControl
    {
        private static readonly int columns = 6;
        private static readonly int pageSize = 30;

        private readonly Dataset dataSet;
        private readonly UserControl layout;
        private readonly Button nextPage, prevPage;

        public event EventHandler MeshClicked;

        public DataItem SelectedMesh { get; private set; }
        public int Pages => this.dataSet.Count / pageSize;
        public int Page { get; private set; }

        public MeshOverview(Dataset dataSet)
        {
            this.InitializeComponent();

            this.dataSet = dataSet;
            this.Page = 0;

            this.layout = new UserControl
            {
                ClientSize = new Size(this.ClientSize.Width, this.ClientSize.Height - 80),
                Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom | AnchorStyles.Left,
                AutoScroll = true
            };

            this.nextPage = new Button
            {
                Text = "Next",
                Location = new Point(this.ClientSize.Width - 110, this.ClientSize.Height - 60),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right
            };

            this.nextPage.Click += (sender, e) => this.LoadPage(this.Page + 1);

            this.prevPage = new Button
            {
                Text = "Previous",
                Location = new Point(this.ClientSize.Width - 190, this.ClientSize.Height - 60),
                Anchor = AnchorStyles.Bottom | AnchorStyles.Right,
            };

            this.prevPage.Click += (sender, e) => this.LoadPage(this.Page - 1);

            this.Controls.Add(this.layout);
            this.Controls.Add(this.nextPage);
            this.Controls.Add(this.prevPage);

            this.Resize += (sender, e) => this.UpdateLayout();

            this.LoadPage(0);
        }

        public void LoadPage(int page)
        {
            this.Page = page = MathHelper.Clamp(page, 0, this.Pages);

            for (int i = 0; i < this.layout.Controls.Count; i++)
            {
                this.layout.Controls[i].Dispose();
            }

            this.layout.Controls.Clear();

            for (int i = 0; i < pageSize; i++)
            {
                int index = page * pageSize + i;

                if (index >= this.dataSet.Count)
                {
                    this.UpdateLayout();
                    return;
                }

                DataItem mesh = this.dataSet[index];

                var box = new PictureBox()
                {
                    Size = new Size(100, 100),
                    SizeMode = PictureBoxSizeMode.StretchImage,
                    Image = mesh.LoadThumbnail(),
                };

                if (mesh.Distance >= 0)
                {
                    new ToolTip().SetToolTip(box, mesh.Label + "\nDistance: " + mesh.Distance);
                }
                else
                {
                    new ToolTip().SetToolTip(box, mesh.Label);
                }

                box.Click += (sender, e) => this.ImageClicked(mesh);

                this.layout.Controls.Add(box);
            }

            this.UpdateLayout();
        }

        private void UpdateLayout()
        {
            int width = (this.layout.Width - 40) / columns;

            for (int i = 0; i < this.layout.Controls.Count; i++)
            {
                int x = i % columns;
                int y = i / columns;

                this.layout.Controls[i].Size = new Size(width - 10, width - 10);
                this.layout.Controls[i].Location = new Point(20 + width * x, 20 + width * y);
            }
        }

        private void ImageClicked(DataItem mesh)
        {
            this.SelectedMesh = mesh;

            if (this.MeshClicked != null)
            {
                this.MeshClicked(this, EventArgs.Empty);
            }
        }
    }
}
