import sys, mplcursors
import numpy as np
import matplotlib.pyplot as plt

from sklearn.manifold import TSNE


def main(data, label):
    X = np.loadtxt(data)
    labels = np.loadtxt(label, dtype='str')
    label_set = sorted(set(labels))
    colours = np.random.random_sample((len(label_set), 3))
    print(label_set)

    # Run t-SNE using the recommended default settings
    Y = TSNE().fit_transform(X)

    _, ax = plt.subplots()
    for i in range(len(label_set)):
        # Find all indices for the current label
        l = label_set[i]
        indices = np.where(labels == l)
        ax.scatter([Y[i, 0] for i in indices], [Y[i, 1] for i in indices], s=20, c=[colours[i]], label=l)

    # Create annotations on hover
    mplcursors.cursor(hover=True)

    # Create the legend and position it outside the graph
    ax_size = ax.get_position()
    ax.set_position([ax_size.x0, ax_size.y0, ax_size.width*0.8, ax_size.height])
    legend = ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=3)
    ax.add_artist(legend)

    plt.show()

if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])