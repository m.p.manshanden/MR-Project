import cv2, math, util, json
import numpy as np

from pathlib import Path
from multiprocessing import Process, JoinableQueue, Manager
from skimage import morphology, measure
from queue import Empty

from dataset.files import remove_entry

def extract_all_parallel(queue: 'JoinableQueue', vectors, num_threads=8):
    
    threads = []
    pdict = Manager().dict()
    args = (queue, pdict)

    for i in range(num_threads):
        thread = Process(target=extract_all, args=args)
        thread.daemon = True
        thread.start()
        threads.append(thread)

    for t in threads:
        t.join()

    vectors.update(pdict)    


def extract_all(queue: 'JoinableQueue', vectors):
    off_file = None

    try: off_file = queue.get_nowait()
    except Empty: return

    while True:
        off_file = Path(off_file)
        name = off_file.parent.stem

        silhouette1 = off_file.parent.joinpath("silhouette1.png").as_posix()
        silhouette2 = off_file.parent.joinpath("silhouette2.png").as_posix()

        try:
            vectors[name] = extract_features([silhouette1, silhouette2])
        except:
            util.error("Could not extract features for " + name + ", deleting entry from dataset...")
            remove_entry(off_file.parent)

        util.log("Extracted " + name)

        try: off_file = queue.get_nowait()
        except Empty: break

    return vectors


def extract_features(silhouttes):
    features = [process_silhouette(s) for s in silhouttes]
    all_features = concat_silhouette_vectors(features)
    return all_features

def process_silhouette(silhouette_file):
    # open file and convert to binary
    cv_img = cv2.imread(silhouette_file, cv2.IMREAD_GRAYSCALE)
    _, cv_img = cv2.threshold(cv_img, 127, 255, cv2.THRESH_BINARY_INV)

    # get contour data
    contours, hierarchy = cv2.findContours(cv_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # Only select non-zero contour
    contour = next(c for c in contours if cv2.contourArea(c) > 0)

    prop_img = cv2.bitwise_not(cv_img)
    props = measure.regionprops(prop_img)[0]
    
    area = props.area
    perimeter = props.perimeter
    compactness = perimeter * perimeter / (4 * math.pi * area)
    xmin, ymax, w, h = cv2.boundingRect(contour)
    rectangularity = float(w) / float(h)
    diameter = props.major_axis_length
    eccentricity = props.eccentricity
    solidity = props.solidity
    skeleton = calculate_skeleton(prop_img)

    return {
        "area" : area, 
        "perimeter" : perimeter, 
        "compactness" : compactness, 
        # "aabb_xmin" : xmin, 
        # "aabb_ymin" : ymax - h, 
        # "aabb_xmax" : xmin + w,
        # "aabb_ymax" : ymax, 
        "rectangularity" : rectangularity, 
        "diameter" : diameter, 
        "eccentricity" : eccentricity, 
        "solidity" : solidity,
        "skeleton" : skeleton
    }


def concat_silhouette_vectors(silhouette_vectors):
    result = {}
    i = 0

    keys = silhouette_vectors[0].keys()

    for vector in silhouette_vectors:
        for key in keys: 
            new_key = key + str(i)
            result[new_key] = vector[key]
        i += 1

    return result


def calculate_skeleton(silhoutte):
    silhoutte[silhoutte > 1] = 1
    skeleton = morphology.skeletonize(silhoutte)
    return int(np.sum(skeleton))


def get_avg_stddev(vectors):
    avg = {}
    stddev = {}

    for feature in util.get_first(vectors).keys():
        # Determine the average and stddev
        values = [v[feature] for v in vectors.values()]
        avg[feature] = np.average(values)
        stddev[feature] = np.std(values)

    return avg, stddev

def normalize_vector(vector, avg, stddev):
    for feature in vector.keys():
        vector[feature] = (vector[feature] - avg[feature]) / stddev[feature]

    return vector


def normalize_vectors(vectors):
    avg, stddev = get_avg_stddev(vectors)

    for key, val in vectors.items():
        vectors[key] = normalize_vector(val, avg, stddev)

    return vectors, avg, stddev
