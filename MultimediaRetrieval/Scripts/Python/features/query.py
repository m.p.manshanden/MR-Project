import util, json

from pathlib import Path
from annoy import AnnoyIndex


def index_to_key_map(dictionary):
    map = {}
    reverse_map = {}
    i = 0

    for key in dictionary.keys():
        map[i] = key
        reverse_map[key] = i
        i += 1

    return map, reverse_map

def create_ann_clustering(vectors, md5_to_int):
    keys = util.get_first(vectors).keys()
    key_count = len(keys)

    # Create the index and fill it
    ann = AnnoyIndex(key_count, "euclidean")

    for md5, vector in vectors.items():
        vector = list(vector.values())
        index = md5_to_int(md5)
        ann.add_item(index, vector)

    # Build the trees using the class count for the amount
    ann.build(key_count)

    return ann

def query_ann(vector, ann_object, int_to_md5, n=10):
    # Get the n nearest meshes
    (meshes, distances) = ann_object.get_nns_by_vector(list(vector.values()), n, include_distances=True)
    
    meshes = map(int_to_md5, meshes)
    meshes = list(meshes)

    return json.dumps({"meshes": meshes, "distances": distances})



