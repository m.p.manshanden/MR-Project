import hashlib, sys
from pathlib import Path
from util import get_md5
import json

def read_class_file(class_file):
    """
    Parses a .cla file into a dictionary with for every filenr an entry
    with the class tree, starting with the most descriptive.
    
    For example: dict["1243"] = "sedan/car/vehicle"
    """
    label_dir = {}
    labels = {}

    with open(class_file, 'r') as f:
        current = ""
        lines = f.readlines()

        for line in lines[2:]:

            line = line.strip()

            if len(line) == 0:
                line = f.readline()
                continue

            if line.isdigit():
                labels[line] = current

            else:
                parts = line.split(' ')

                if not parts[1].isdigit():
                    key = parts[1]
                    val = parts[0]
                    label_dir[val] = val + "/" + label_dir[key]
                    current = label_dir[val]
                else:
                    val = parts[0]
                    label_dir[val] = val
                    current = val

            line = f.readline()

    return labels

def extract_labels(directory, class_file):
    class_labels = read_class_file(class_file)
    print(class_labels)
    labels = {}

    for file in Path(directory).glob("**/*.off"):
        filenr = file.stem[1:]

        if filenr not in class_labels:
            continue

        off = file.as_posix()
        md5 = get_md5(off)

        labels[md5] = class_labels[filenr]

    return labels
        
    

def main():
    arg1 = sys.argv[1] # path to directory containing .off files
    arg2 = sys.argv[2] # path to .cla file
    dir1 = Path.cwd().joinpath(arg1)
    dir2 = Path.cwd().joinpath(arg2)
    labels = extract_labels(dir1.as_posix(), dir2.as_posix())
    
    with open('princeton_labels_' + dir2.stem + '.json', 'w') as f:
        json.dump(labels, f, indent=4, sort_keys=True)

if __name__ == "__main__":
    main()
