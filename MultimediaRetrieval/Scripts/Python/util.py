import os, sys, hashlib, json
from pathlib import Path
import numpy as np

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)

def log(message):
    """
    Writes a message to stdout and flushes immediately.
    """
    sys.stdout.write(message + "\n")
    sys.stdout.flush()

def error(message):
    """
    Logs an error.
    """
    sys.stderr.write(message + "\n")
    sys.stderr.flush()

def create_dir_if_not_exists(path):
    """
    Creates a directory only if it does not exists yet.
    """
    if isinstance(path, Path):
        path = path.as_posix()

    if not os.path.exists(path):
        os.makedirs(path)

def write_file(path, value):
    """
    Writes value to a file.
    """
    if isinstance(path, Path):
        path = path.as_posix()

    with open(path, "w") as f:
        value = str(value)
        f.write(value)

def write_json(path, value):
    """
    Writes an object in json-format to a file.
    """
    if isinstance(path, Path):
        path = path.as_posix()

    with open(path, "w") as f:
        json.dump(value, f, cls=NpEncoder)

def read_file(path):
    """
    Reads lines from a file.
    """
    if isinstance(path, Path):
        path = path.as_posix()

    with open(path, "r") as f:
        lines = f.readlines()
        return lines

def read_json(path):
    if isinstance(path, Path):
        path = path.as_posix()

    with open(path, "r") as f:
        return json.load(f)

def read_first_int(file):
    """
    Reads the first integer that appears in the file, or None if 
    no integers appears.

    Useful for finding the number of vertices in a .OFF mesh.
    """
    for line in read_file(file):
        parts = line.split(' ')
        for part in parts:
            if part.isdigit():
                return int(part)

    return None


def get_md5(file):
    """
    Returns the md5 hash of a file.
    """
    md5 = hashlib.md5()

    with open(file, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            md5.update(chunk)

    return md5.hexdigest()
        
def get_first(dictionary):
    return next(iter(dictionary.values()))

