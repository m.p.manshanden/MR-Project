import sys, signal, util

from multiprocessing import JoinableQueue

from dataset.silhouette import SilhouetteRenderer
from dataset.thumbnail import ThumbnailRenderer
from dataset.files import scan_directory
from dataset.files import process_off_files
from dataset.files import process_off_files_parallel
from graphics.window import Window

def create_silhouette_renderer(size=256):
    """
    Returns both the window rendering silhouettes and the silhouette
    renderer itself.
    """
    queue = JoinableQueue()
    window = Window(size, size, "slave_silh", SilhouetteRenderer(size, queue), visible=False)
    return window, queue


def create_thumbnail_renderer(size=256):
    """
    Returns both the window rendering thumbnails and the thumbnail
    renderer itself.
    """ 
    queue = JoinableQueue()
    window = Window(size, size, "slave_thumb", ThumbnailRenderer(size, queue), visible=False)
    return window, queue

def main(dir_in, dir_out):
    w1, silh = create_silhouette_renderer()
    w2, thmb = create_thumbnail_renderer()

    w1.open_async()
    w2.open_async()

    files = None
    average = 0

    if len(sys.argv) == 4 and sys.argv[3].isdigit():
        scan_count = int(sys.argv[3])
        files, average = scan_directory(dir_in, scan_count)
        process_off_files(files, average, dir_out, silh, thmb)
    else:
        files, average = scan_directory(dir_in)
        process_off_files_parallel(files, average, dir_out, silh, thmb)

    silh.join()
    thmb.join()

    w1.close()
    w2.close()


if __name__ == "__main__":
    signal.signal(signal.SIGINT, lambda signum, frame: sys.exit())
    signal.signal(signal.SIGTERM, lambda signum, frame: sys.exit())

    main(sys.argv[1], sys.argv[2])
