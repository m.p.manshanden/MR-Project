import sys, signal, util, re

from multiprocessing import JoinableQueue

from dataset.files import *
from dataset.silhouette import SilhouetteRenderer
from features import extract, query
from graphics.window import Window


def extract_features(location):
    vectors_json_path = Path(location).joinpath("vectors.json")
    stats_json_path = Path(location).joinpath("stats.json")
    metadata_path = Path(location).joinpath("metadata")

    vectors = {}
    target_count = util.read_first_int(metadata_path)

    if vectors_json_path.exists() and stats_json_path.exists():
        vectors = util.read_json(vectors_json_path)
        stats = util.read_json(stats_json_path)

        return vectors, stats["avg"], stats["stddev"], target_count

    # find mesh files and extract their features
    files, target_count = scan_directory(location)    
    extract.extract_all_parallel(files, vectors)

    # normalize the feature vectors
    vectors, avg, stddev = extract.normalize_vectors(vectors)

    write_json(vectors_json_path, vectors)
    write_json(stats_json_path, { "avg" : avg, "stddev" : stddev })

    return vectors, avg, stddev, target_count



def main(location):
    # create working directory for saving silhouettes
    util.create_dir_if_not_exists("./workdir")

    # create renderer for silhouettes
    silhouette_queue = JoinableQueue()
    Window(256, 256, "slave_query", SilhouetteRenderer(256, silhouette_queue), visible=False).open_async()

    # extract featurs
    vectors, avg, stddev, target_count = extract_features(location)

    # mappings for md5 to int and vice versa
    md5_lookup, int_lookup = query.index_to_key_map(vectors)
    int_to_md5 = lambda i: md5_lookup[i]
    md5_to_int = lambda h: int_lookup[h]

    # prepare ann for lookup
    ann = query.create_ann_clustering(vectors, md5_to_int)

    # Notify host that we are done loading
    util.log("Loaded")

    regex = Path(location).resolve().as_posix() + "/[a-f0-9]{32}/mesh.off"

    while True:
        inp = input().split(' ')

        off = inp[0]
        count = inp[1]

        off = Path(off).resolve().as_posix()
        md5 = util.get_md5(off)

        count = int(count)

        if re.match(regex, off):

            md5 = off[-41:-9]
            output = query.query_ann(vectors[md5], ann, int_to_md5, count)

            util.log(output)

        else:

            # process off files
            process_off_file(off, target_count, "./workdir/", silhouette_queue, None)
            silhouette_queue.join()

            # extract features
            silhouettes = ["./workdir/" + md5 + "/silhouette1.png", "./workdir/" + md5 + "/silhouette2.png"]
            feature_vector = extract.extract_features(silhouettes)
            feature_vector = extract.normalize_vector(feature_vector, avg, stddev)

            # query the ann and generate output
            output = query.query_ann(feature_vector, ann, int_to_md5, count)

            util.log(output)



if __name__ == "__main__":
    signal.signal(signal.SIGINT, lambda signum, frame: sys.exit())
    signal.signal(signal.SIGTERM, lambda signum, frame: sys.exit())

    main(sys.argv[1])
