import hashlib, sys
from pathlib import Path
import json
from util import get_md5

def extract_labels(directory):
    labels = {}

    for file in Path(directory).glob("**/*.off"):
       md5 = get_md5(file.as_posix()) 
       labels[md5] = file.parent.name

    return labels       


def main():
    arg = sys.argv[1] # path to directoy containing .off files
    dir = Path.cwd().joinpath(arg).as_posix()
    labels = extract_labels(dir)
    
    with open('umass_lables.json', 'w') as f:
        f.write(json.dumps(labels, indent=4, sort_keys=True))

if __name__ == "__main__":
    main()
