import glm, time
import open3d as o3d
import numpy as np

from multiprocessing import JoinableQueue

import graphics.program
import graphics.vbo
import graphics.ebo
import graphics.vao
import graphics.input
import graphics.camera
import graphics.renderable
import graphics.utils
import graphics.window


VERT_CODE = """
#version 330 core

uniform mat4 Projection;
uniform mat4 View;

layout(location = 0) in vec3 v_Position;
layout(location = 1) in vec3 v_Normal;

out vec3 f_Position;
out vec3 f_Normal;

void main()
{
    f_Position = v_Position;
    f_Normal = v_Normal;

    gl_Position = Projection * View * vec4(v_Position, 1);
}
"""

FRAG_CODE = """
#version 330 core

uniform vec3 light_Color;
uniform vec3 light_Direction;
uniform float light_MinIntensity;

in vec3 f_Position;
in vec3 f_Normal;

out vec4 out_Color;

vec4 lambert(vec3 lightDirection, vec3 lightColor, float minValue)
{
    lightDirection = normalize(lightDirection);
    float nDotL = dot(lightDirection, f_Normal);
    float intensity = abs(nDotL) * (1.0 - minValue) + minValue;
    return vec4(intensity * lightColor, 1);
}

void main()
{
    out_Color = lambert(light_Direction.xyz, light_Color.rgb, light_MinIntensity);
}
"""


class ThumbnailRenderer(graphics.renderable.Renderable):
    def __init__(self, size, queue):
        self.size = size
        self.queue = queue

    def enqueue(self, off_file, out_dir):
        out_dir = out_dir.as_posix()
        self.queue.put((off_file, out_dir))

    def wait_finished(self):
        self.queue.join()

    def initialize(self, window: 'graphics.window.Window'):
        self.window = window

        # compile shaders for solid rendering
        self.program = graphics.program.Program()
        self.program.compile(VERT_CODE, FRAG_CODE)

        # vbos for mesh data
        self.pos_vbo = graphics.vbo.Vbo(self.program)
        self.pos_vbo.add_float_attribute("v_Position", 3)
        self.norm_vbo = graphics.vbo.Vbo(self.program)
        self.norm_vbo.add_float_attribute("v_Normal", 3)

        # index buffer
        self.ebo = graphics.ebo.Ebo()

        # create vao for bindings (required opengl 3.3+)
        self.vao = graphics.vao.Vao()
        self.vao.add_vbo(self.pos_vbo)
        self.vao.add_vbo(self.norm_vbo)
        self.vao.set_ebo(self.ebo)

        self.clear_color = glm.vec3(0.1)
        self.light_color = glm.vec3(0.9)
        self.light_direction = glm.vec3(1, 0, 1)
        self.light_min_intensity = 0.3

        # fbo for rendering the thumbnail to
        self.fbo = graphics.fbo.Fbo(self.size, self.size, self.clear_color)

        self.camera = graphics.camera.Camera(self.size, self.size)


    def load_mesh(self, path):
        """
        Loads a mesh file for rendering.
        """
        mesh = o3d.io.read_triangle_mesh(path)        
        (vertices, normals, indices) = graphics.utils.get_mesh_render_data_smooth(mesh)
        self.pos_vbo.buffer_data(vertices)
        self.norm_vbo.buffer_data(normals)
        self.ebo.buffer_data(indices)

        center = mesh.get_center()

        self.camera.set_lookat(center[0], center[1], center[2])
        self.camera.set_phi(-0.7)
        self.camera.set_theta(1.060)
        self.camera.set_distance(1.8)

    def draw(self):
        if self.queue.empty():
            time.sleep(1)
            return

        (off, out_path) = self.queue.get()
        self.load_mesh(off)

        self.vao.bind()

        self.window.set_clear_color(self.clear_color)

        self.program.use()
        self.program.uniform["Projection"] = self.camera.projection_matrix
        self.program.uniform["View"] = self.camera.view_matrix
        self.program.uniform["light_Color"] = self.light_color
        self.program.uniform["light_Direction"] = self.light_direction
        self.program.uniform["light_MinIntensity"] = self.light_min_intensity

        self.vao.draw_to_fbo(self.fbo)

        image = self.fbo.get_image()
        image.save(out_path + "/thumbnail.png")
        image.close()

        self.queue.task_done()

