import os, sys

from pathlib import Path
from multiprocessing import Process, JoinableQueue
from queue import Empty

import numpy as np
import open3d as o3d

from util import *
from dataset.mesh import *

def scan_directory(directory: str, max_count=-1):
    """
    Scans a directory for .OFF files and returns two values:
    a queue of every path to a .OFF file and an integer with the
    average vertex count of the .OFF file meshes.
    """
    queue = JoinableQueue()
    vertex_count = []

    count = 0

    for file in Path(directory).glob("**/*.off"):
        off = file.as_posix()

        log("Found " + off)

        vcount = read_first_int(off)
    
        queue.put(off)     
        vertex_count.append(vcount)

        count += 1

        if max_count > 0 and count >= max_count:
            break

    if count == 0:
        error("No meshes found in " + directory)
        return queue, 0

    average = np.mean(vertex_count)
    average = int(average)

    log("Scan complete: " + str(count) + " meshes found, " + str(average) + " vertex average")

    return queue, average


def process_off_files_parallel(queue, target_vertex_count, out_dir, silhouette_q, thumbnail_q, num_threads=8):
    """
    Processes a queue of paths to .OFF files, parallelly.
    """

    threads = []
    args = (queue, target_vertex_count, out_dir, silhouette_q, thumbnail_q)

    for i in range(num_threads):
        thread = Process(target=process_off_files, args=args)
        thread.daemon = True
        thread.start()
        threads.append(thread)

    for t in threads:
        t.join()


def process_off_files(queue, target_vertex_count, out_dir, silhouette_q, thumbnail_q):
    """
    Processes a queue of paths to .OFF files.
    """

    off_file = None

    try: off_file = queue.get_nowait()
    except Empty: return

    while True:
        try: 
            process_off_file(off_file, target_vertex_count, out_dir, silhouette_q, thumbnail_q)
        except:
            error("Could not process " + off_file)

        queue.task_done()

        log("Processed " + off_file)

        try: off_file = queue.get_nowait()
        except Empty: break

    write_file(Path(out_dir).joinpath("metadata"), target_vertex_count)   

        
def process_off_file(off_file, target_vertex_count, out_dir, silhouette_q, thumbnail_q):
    """
    """

    out_dir = Path(out_dir)
    
    # create dir that will contain all data for this mesh
    md5 = get_md5(off_file)
    out_dir = out_dir.joinpath(md5)

    # load the mesh file
    mesh = o3d.io.read_triangle_mesh(off_file)

    # process mesh file
    mesh = clean_mesh(mesh)
    mesh = normalize_mesh(mesh)

    # write processed mesh file
    mesh_path = out_dir.joinpath('mesh.off')
    create_dir_if_not_exists(out_dir)
    o3d.io.write_triangle_mesh(mesh_path.as_posix(), mesh)

    if silhouette_q is not None: process_silhouettes(mesh_path, silhouette_q)
    if thumbnail_q is not None: process_thumbnail(mesh_path, thumbnail_q)

    # save metadata for this mesh
    with open(out_dir.joinpath('metadata').as_posix(), "w") as f:
        vertex_count = len(mesh.vertices)
        vertex_diff = vertex_count - target_vertex_count

        f.write(str(target_vertex_count) + "\n")
        f.write(str(vertex_count) + "\n")
        f.write(str(vertex_diff) + "\n")
        f.write(off_file)


def process_silhouettes(mesh_path, silhouette_queue):
    # output directory
    out_dir = mesh_path.parent

    # paths to silhouete images
    silh1_path = out_dir.joinpath('silhouette1.png')
    silh2_path = out_dir.joinpath('silhouette2.png')

    if not silh1_path.exists() or not silh2_path.exists():
        silhouette_queue.put(( mesh_path.as_posix(), out_dir.as_posix() ))


def process_thumbnail(mesh_path, thumbnail_queue):
    # output directory
    out_dir = mesh_path.parent

    if not out_dir.joinpath('thumbnail.png').exists():
        thumbnail_queue.put(( mesh_path.as_posix(), out_dir.as_posix() ))


def remove_entry(path):
    path = Path(path)

    path.joinpath("mesh.off").unlink()
    path.joinpath("silhouette1.png").unlink()
    path.joinpath("silhouette2.png").unlink()
    path.joinpath("thumbnail.png").unlink()
    path.joinpath("metadata").unlink()
    path.rmdir()
