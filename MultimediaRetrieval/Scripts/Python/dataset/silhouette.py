import glm, time
import open3d as o3d
import numpy as np
import OpenGL.GL as gl

from multiprocessing import JoinableQueue

import graphics.fbo
import graphics.program
import graphics.vbo
import graphics.ebo
import graphics.vao
import graphics.renderable

VERT_CODE = """
#version 330 core

uniform mat4 Projection;
uniform mat4 View;

layout(location = 0) in vec3 v_Position;

void main()
{
    gl_Position = Projection * View * vec4(v_Position, 1);
}
"""

FRAG_CODE = """
#version 330 core

out vec4 out_Color;

void main()
{
    out_Color = vec4(0, 0, 0, 1);
}

"""


class SilhouetteRenderer(graphics.renderable.Renderable):
    def __init__(self, size, queue):
        self.size = size
        self.queue = queue

    def enqueue(self, off_file, out_dir):
        out_dir = out_dir.as_posix()
        self.queue.put((off_file, out_dir))

    def wait_finished(self):
        self.queue.join()

    def initialize(self, window):
        self.window = window
        self.program = graphics.program.Program()
        self.program.compile(VERT_CODE, FRAG_CODE)

        self.vbo = graphics.vbo.Vbo(self.program)
        self.vbo.add_float_attribute("v_Position", 3)

        self.ebo = graphics.ebo.Ebo()

        self.vao = graphics.vao.Vao()
        self.vao.add_vbo(self.vbo)
        self.vao.set_ebo(self.ebo)

        self.fbo = graphics.fbo.Fbo(self.size, self.size, glm.vec3(1))

        self.program.use()
        self.program.uniform["Projection"] = glm.ortho(-0.5, 0.5, -0.5, 0.5, 0.0, 10.0)

    def draw(self):
        if self.queue.empty():
            time.sleep(1)
            return

        (off, out_path) = self.queue.get()
        mesh = o3d.io.read_triangle_mesh(off)

        vertices = np.asarray(mesh.vertices)
        vertices = np.float32(vertices).tobytes()

        indices = np.asarray(mesh.triangles).tobytes()

        self.vbo.buffer_data(vertices)
        self.ebo.buffer_data(indices)

        self.vao.bind()

        # project along x-axis
        self.program.uniform["View"] = glm.lookAt(glm.vec3(0, 0, -5), glm.vec3(0), glm.vec3(0, 1, 0))
        self.vao.draw_to_fbo(self.fbo)

        image = self.fbo.get_image()
        image.save(out_path + "/silhouette1.png")
        image.close()

        # project along y-axis
        self.program.uniform["View"] = glm.lookAt(glm.vec3(0, 5, 0), glm.vec3(0), glm.vec3(0, 0, 1))
        self.vao.draw_to_fbo(self.fbo)

        image = self.fbo.get_image()
        image.save(out_path + "/silhouette2.png")
        image.close()

        # project along z-axis
        # self.program.uniform["View"] = glm.lookAt(glm.vec3(5, 0, 0), glm.vec3(0), glm.vec3(0, 1, 0))
        # self.vao.draw_to_fbo(self.fbo)
        
        # image = self.fbo.get_image()
        # image.save(out_path + "/silhouette3.png")
        # image.close()

        self.queue.task_done()

