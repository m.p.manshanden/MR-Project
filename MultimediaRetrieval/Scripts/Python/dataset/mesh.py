import hashlib, os, sys

from pathlib import Path
from queue import Empty
from scipy import linalg

import numpy as np
import open3d as o3d


def clean_mesh(mesh):
    """
    Returns a copy of the mesh without redundant data.
    """
    # Remove redundant data from the mesh
    mesh = mesh.remove_degenerate_triangles()
    mesh = mesh.remove_duplicated_triangles()
    mesh = mesh.remove_duplicated_vertices()
    mesh = mesh.remove_non_manifold_edges()
    mesh = mesh.remove_unreferenced_vertices()

    return mesh


def resample_mesh(mesh, target_vertex_count):
    """
    Resamples the mesh such that its number of vertices is reduced
    or increased to the target_vertex_count.
    """
    while len(mesh.vertices) < target_vertex_count:
        mesh = mesh.subdivide_midpoint()

    # Turns out that the vertex count must be multiplied by 2 to get
    # the triangle count and thus the target count.
    mesh = mesh.simplify_quadric_decimation(target_vertex_count * 2)

    return mesh

def normalize_vector(vector):
    norm = np.linalg.norm(vector)

    if norm == 0: 
       return vector

    return vector / norm

def realign_mesh(mesh):
    # Translate the barycenter to the origin
    barycenter = get_mesh_barycenter(mesh)
    mesh = mesh.translate(-barycenter)

    # Align with coordinate frame
    values, vectors = principal_component_analysis(np.asarray(mesh.vertices))
    max_index = np.argmax(values)
    min_index = np.argmin(values)
    med_index = 3 - max_index - min_index

    rotation = [ calculate_angle([1,0,0], vectors[max_index]) ,
                 calculate_angle([0,1,0], vectors[med_index]) ,
                 calculate_angle([0,0,1], vectors[min_index]) ]

    if np.any(rotation):
        mesh = mesh.rotate(rotation, type=o3d.geometry.RotationType.AxisAngle)

    return mesh

def realign_mesh_obb(mesh):
    # get oriented bounding box
    obb = mesh.get_oriented_bounding_box()

    # to orthonormal matrix
    x_axis = normalize_vector(obb.x_axis)
    y_axis = normalize_vector(obb.y_axis)
    z_axis = normalize_vector(obb.z_axis)

    # rotate using orthonormal matrix
    rotation = np.array([[x_axis[0], x_axis[1], x_axis[2], 0],
                         [y_axis[0], y_axis[1], y_axis[2], 0],
                         [z_axis[0], z_axis[1], z_axis[2], 0],
                         [0        , 0        , 0        , 1]])

    return mesh.transform(rotation)

def normalize_mesh(mesh):
    """
    Normalizes the mesh to a consistent as possible output. 
    """
    
    # Align with coordinate frame
    mesh = realign_mesh_obb(mesh)
    # Flip based on moment test
    mesh = moment_test(mesh)
    # Scale to unit volume
    mesh = scale_mesh(mesh)

    return mesh

def scale_mesh(mesh):
    """
    Scales the mesh so that it fits in the unit volume.
    """
    min_bounds = mesh.get_min_bound()
    max_bounds = mesh.get_max_bound()

    scale = 1.0 / (max_bounds - min_bounds).max()
    scale_matrix = np.array([[scale, 0, 0, 0],
                             [0, scale, 0, 0],
                             [0, 0, scale, 0],
                             [0, 0, 0, 1]])

    mesh = mesh.translate(-min_bounds)
    mesh = mesh.transform(scale_matrix)
    mesh = mesh.translate([-0.5, -0.5, -0.5])

    return mesh

def get_mesh_barycenter(mesh):
    """
    Calculates and returns the 3D centroid (barycenter) of a 3D mesh.
    """
    faces = np.asarray(mesh.triangles)
    vertices = np.asarray(mesh.vertices)

    centers = np.asarray([0, 0, 0])
    area = 0
    
    for face in faces:
        triangle = [vertices[i] for i in face]
        centers = centers + triangle_center(triangle)
        area = area + triangle_area(triangle)

    return centers / area

def principal_component_analysis(vertices):
    """
    Performs principal component analysis on a given set of 
    vertices (point cloud), and returns two values: the 
    eigenvalues for every axis and the eigenvectors for every
    axis.
    """
    # Determine the covariance matrix of the given vertices
    covariance = np.cov(vertices.T)

    # Compute the eigenvalues and eigenvectors of the covariance matrix
    (eigenvalues, eigenvectors) = linalg.eigh(covariance)

    return eigenvalues, eigenvectors

def calculate_angle(v1, v2):
    """
    Returns the angle between two given vectors.
    """
    v1_u = linalg.norm(v1)
    v2_u = linalg.norm(v2)
    
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def moment_test(mesh):
    """
    Flips a mesh based on its moment, for every axis.
    """
    # Move the bulk of the vertices to the 'left' (all coords negative)
    f_x = 0
    f_y = 0
    f_z = 0
    for tri in mesh.triangles:
        center = triangle_center([mesh.vertices[i] for i in tri])
        f_x = f_x + np.sign(center[0]) * center[0] * center[0]
        f_y = f_y + np.sign(center[1]) * center[1] * center[1]
        f_z = f_z + np.sign(center[2]) * center[2] * center[2]

    # Flip the mesh based on the calculated signs
    F = [[np.sign(f_x), 0, 0, 0],
         [0, -1 * np.sign(f_y), 0, 0],
         [0, 0, np.sign(f_z), 0],
         [0, 0, 0, 1]]

    return mesh.transform(F)

def triangle_center(vertices):
    return [(vertices[0][0] + vertices[1][0] + vertices[2][0]) / 3,
            (vertices[0][1] + vertices[1][1] + vertices[2][1]) / 3,
            (vertices[0][2] + vertices[1][2] + vertices[2][2]) / 3]

def triangle_area(vertices):
    a = vertices[0][0] * (vertices[1][1] - vertices[2][2])
    b = vertices[1][0] * (vertices[2][1] - vertices[0][2])
    c = vertices[2][0] * (vertices[0][1] - vertices[1][2])
    return np.absolute(a + b + c) / 2


