import glfw

import graphics.window

class Input:
    def __init__(self):
        self._window = None
        self._is_initialized = False
        self._mouse_position_x = 0
        self._mouse_position_y = 0
        self._scroll_delta = 0
        self._mouse_delta_x = 0
        self._mouse_delta_y = 0

    def initialize(self, window: 'graphics.window.Window'):
        if self._is_initialized:
            return

        self._window = window.get_handle()

        # configure callbacks
        glfw.set_mouse_button_callback(self._window, self._on_mousebutton)
        glfw.set_key_callback(self._window, self._on_keyboard)
        glfw.set_scroll_callback(self._window, self._on_scroll)

        # initialize mouse state
        mouse_postion = glfw.get_cursor_pos(self._window)
        self._mouse_position_x = mouse_postion[0]
        self._mouse_position_y = mouse_postion[1]

        self._is_initialized = True

    def update(self):
        pos = glfw.get_cursor_pos(self._window)
        self._mouse_delta_x = pos[0] - self._mouse_position_x
        self._mouse_delta_y = pos[1] - self._mouse_position_y
        self._mouse_position_x = pos[0]
        self._mouse_position_y = pos[1]

    def next_frame(self):
        self._scroll_delta = 0

    def _on_scroll(self, win, q, delta):
        self._scroll_delta = self._scroll_delta + delta

    def _on_mousebutton(self, win, button, action, mods):
        pass

    def _on_keyboard(self, win, key, scancode, action, mods):
        pass

    def get_left_mouse_button(self):
        return glfw.get_mouse_button(self._window, glfw.MOUSE_BUTTON_LEFT)

    def get_mouse_delta(self):
        return (self._mouse_delta_x, self._mouse_delta_y)

    def get_scroll_delta(self):
        return self._scroll_delta
