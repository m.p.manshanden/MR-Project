import glfw, glm
import OpenGL.GL as gl
import open3d as o3d
import numpy as np


def get_mesh_render_data_smooth(mesh):
    mesh.compute_vertex_normals()

    # data for positions buffer
    vertices = np.asarray(mesh.vertices)
    vertices = np.float32(vertices).tobytes()

    # data for normals buffer
    normals = np.asarray(mesh.vertex_normals)
    normals = np.float32(normals).tobytes()

    # data for index buffer
    indices = np.asarray(mesh.triangles).tobytes()

    return (vertices, normals, indices)

def get_mesh_render_data_flat(mesh):
    mesh.compute_triangle_normals()

    # flatten triangle indices and point every index to the x-coordinate only
    tris = np.asarray(mesh.triangles).flatten() * 3
    # add references to the y and z coordinates
    tris = np.column_stack((tris, tris + 1, tris + 2)).flatten()

    # get all vertex positions (xyzxyzxyz)
    vertices = np.asarray(mesh.vertices).flatten()
    # use triangle indices to get sequence of trianlge vertices
    vertices = np.take(vertices, tris)
    vertices = np.float32(vertices).tobytes()

    # data for normals buffer
    normals = np.asarray(mesh.triangle_normals)
    # in flat shading, every three vertices of the triangle have the same normal
    normals = normals.repeat(3, axis=0).flatten()
    normals = np.float32(normals).tobytes()

    # the index data simply points to all vertices in order
    count = len(tris) * 3
    indices = np.arange(count).tobytes()

    return (vertices, normals, indices)
