import glm

def to_cartesian(r, phi, theta):
    return glm.vec3(
        r * glm.sin(theta) * glm.cos(phi),
        r * glm.cos(theta),
        r * glm.sin(theta) * glm.sin(phi)
    )

class Camera:
    def __init__(self, screen_width, screen_height):
        self.distance = 3
        self.theta = glm.pi() / 2.0
        self.phi = 0
        self.lookat = glm.vec3(1)
        self.width = screen_width
        self.height = screen_height
        self.view_matrix = glm.mat4(1)
        self.projection_matrix = glm.mat4(1)
        self._update_view_matrix()
        self._update_projection_matrix()

    def _update_view_matrix(self):
        position = to_cartesian(self.distance, self.phi, self.theta) + self.lookat
        self.view_matrix = glm.lookAt(position, self.lookat, glm.vec3(0, 1, 0))

    def _update_projection_matrix(self):
        if self.width > 0 and self.height > 0:
            self.projection_matrix = glm.perspectiveFov(glm.pi() / 4.0, self.width, self.height, 0.01, 10000)

    def set_phi(self, value):
        self.phi = value
        self._update_view_matrix()

    def phi_add(self, value):
        self.set_phi(self.phi + value)

    def phi_sub(self, value):
        self.set_phi(self.phi - value)

    def set_theta(self, value):
        self.theta = glm.clamp(value, 0.00001, glm.pi() - 0.000001)
        self._update_view_matrix()

    def theta_add(self, value):
        self.set_theta(self.theta + value)

    def theta_sub(self, value):
        self.set_theta(self.theta - value)

    def set_distance(self, value):
        self.distance = glm.max(value, 0.1)
        self._update_view_matrix()

    def distance_add(self, value):
        self.set_distance(self.distance + value)

    def distance_sub(self, value):
        self.set_distance(self.distance - value)

    def set_lookat(self, x, y, z):
        self.lookat = glm.vec3(x, y, z)
        self._update_view_matrix()

    def resize(self, screen_width, screen_height):
        self.width = screen_width
        self.height = screen_height
        self._update_projection_matrix()