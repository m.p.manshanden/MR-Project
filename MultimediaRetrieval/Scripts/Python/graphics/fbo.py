import numpy as np
import OpenGL.GL as gl

import PIL.Image

class Fbo(object):
    def __init__(self, width: int, height: int, clear_color):
        self.width = width
        self.height = height
        self.clear_color = clear_color

        self.handle = gl.glGenFramebuffers(1)
        self.colorbuffer = 0
        self.depthbuffer = 0

        self._create_target()
    
    def _create_target(self):
        if self.colorbuffer == 0:
            self.colorbuffer = gl.glGenTextures(1)
            self.depthbuffer = gl.glGenRenderbuffers(1)

        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, self.handle)

        # configure color back buffer
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.colorbuffer)
        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RGB, self.width, self.height, 0, gl.GL_RGB, gl.GL_UNSIGNED_BYTE, None)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_NEAREST)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_NEAREST)

        # configure depth back buffer
        gl.glBindRenderbuffer(gl.GL_RENDERBUFFER, self.depthbuffer)
        gl.glRenderbufferStorage(gl.GL_RENDERBUFFER, gl.GL_DEPTH_COMPONENT, self.width, self.height)

        # attach to fbo
        gl.glFramebufferTexture2D(gl.GL_FRAMEBUFFER, gl.GL_COLOR_ATTACHMENT0, gl.GL_TEXTURE_2D, self.colorbuffer, 0)
        gl.glFramebufferRenderbuffer(gl.GL_FRAMEBUFFER, gl.GL_DEPTH_ATTACHMENT, gl.GL_RENDERBUFFER, self.depthbuffer)

        if gl.glCheckFramebufferStatus(gl.GL_FRAMEBUFFER) != gl.GL_FRAMEBUFFER_COMPLETE:
            raise Exception("Cannot create fbo")

        gl.glClearColor(self.clear_color.x, self.clear_color.y, self.clear_color.z, 0)
        gl.glEnable(gl.GL_DEPTH_TEST)

        gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
        gl.glBindRenderbuffer(gl.GL_RENDERBUFFER, 0)
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0)


    def bind(self):
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, self.handle)
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)
        gl.glClear(gl.GL_DEPTH_BUFFER_BIT)

    def get_pixels(self):
        gl.glBindTexture(gl.GL_TEXTURE_2D, self.colorbuffer)
        data = gl.glGetTexImage(gl.GL_TEXTURE_2D, 0, gl.GL_RGB, gl.GL_UNSIGNED_BYTE)
        gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
        return data

    def get_image(self):
        pixels = self.get_pixels()
        image = PIL.Image.frombytes("RGB", (self.width, self.height), pixels)
        image = image.transpose(PIL.Image.FLIP_TOP_BOTTOM)
        return image


        

    
