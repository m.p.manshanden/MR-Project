import glfw, glm
import OpenGL.GL as gl
import open3d as o3d
import numpy as np

from multiprocessing import Process, Value
from ctypes import c_bool

import graphics.renderable

class Window:
    def __init__(self, width: int, height: int, title: str, renderable: 'graphics.renderer.Renderable', major=3, minor=3, visible=True):
        self._renderable = renderable
        self._major = major
        self._minor = minor
        self._visible = visible
        self._running = Value(c_bool, True)

        self.width = width
        self.height = height
        self.title = title       

    def _on_size(self, win, width, height):
        self.width = width
        self.height = height
        gl.glViewport(0, 0, width, height)
        self._renderable.resize(width, height)

    def should_close(self):
        if not self._running.value:
            return True
        return glfw.window_should_close(self._handle)

    def get_handle(self):
        return self._handle

    def close(self):
        self._running.value = False

    def set_clear_color(self, color):
        gl.glClearColor(color.x, color.y, color.z, 1)

    def set_wireframe(self, enabled):
        gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE if enabled else gl.GL_FILL)

    def _run(self):
        if not glfw.init():
            raise Exception("Could not initialize glfw.")

        # window settings
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, self._major)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, self._minor)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, 1)
        glfw.window_hint(glfw.VISIBLE, 1 if self._visible else 0)

        # create window
        self._handle = glfw.create_window(self.width, self.height, self.title, None, None)

        glfw.make_context_current(self._handle)        

        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glDepthFunc(gl.GL_LEQUAL)

        self._renderable.initialize(self)
        self._renderable.resize(self.width, self.height)

        glfw.set_window_size_callback(self._handle, self._on_size)

        while not self.should_close():
            glfw.poll_events()

            gl.glClear(gl.GL_COLOR_BUFFER_BIT)
            gl.glClear(gl.GL_DEPTH_BUFFER_BIT)

            self._renderable.update()
            self._renderable.draw()

            glfw.swap_buffers(self._handle)
            
    def open(self):
        t = Process(target=self._run)
        t.daemon = True
        t.start()
        t.join()

    def open_async(self):
        t = Process(target=self._run)
        t.daemon = True
        t.start()