import OpenGL.GL as gl

from array import array

import graphics.program


class Vbo:
    def __init__(self, program: 'graphics.program.Program', source: 'Vbo' = None):
        self.handle = gl.glGenBuffers(1) if source is None else source.handle
        self.program = program
        self.attrib_indices = []
        self.attrib_sizes = []
        self.stride = 0
        self.count = 0
        self.bind()

    def bind(self):
        """
        Binds this instance to the vertex buffer target and enables vertex 
        attributes set by add_float_attribute()
        """
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.handle)

        offset = 0

        for i in range(0, len(self.attrib_indices)):
            gl.glVertexAttribPointer(
                self.attrib_indices[i], 
                self.attrib_sizes[i], 
                gl.GL_FLOAT, 
                gl.GL_FALSE, 
                self.stride, 
                gl.ctypes.c_void_p(offset)
            )

            gl.glEnableVertexAttribArray(self.attrib_indices[i])
            offset += self.attrib_sizes[i]
            
    def add_float_attribute(self, name, size):        
        """
        Adds a floating point attribute. The name must corresponds to 
        name of the 'in variable' in the (vertex) shader. The size is
        the number of components, i.e. 3 for a vec3. 
        """
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.handle)
        index = gl.glGetAttribLocation(self.program.handle, name)

        self.attrib_indices.append(index)
        self.attrib_sizes.append(size)
        self.stride += size * 4
        

    def buffer_data(self, data, size=None):
        """
        Takes a string of bytes or a list and pushes it to the buffer.
        """
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, self.handle)

        if isinstance(data, list):
            data = array('f', data).tostring()
            
        size = len(data) if size is None else size
        gl.glBufferData(gl.GL_ARRAY_BUFFER, size, data, gl.GL_STATIC_DRAW)
        
        self.count = int(size / 4)

