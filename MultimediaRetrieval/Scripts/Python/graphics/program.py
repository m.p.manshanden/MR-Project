import glm

from OpenGL.GL import *
from OpenGL.GL import shaders
from OpenGL.GL import glUseProgram, GL_VERTEX_SHADER, GL_FRAGMENT_SHADER

from array import array

class Uniform:
    def __init__(self, program):
        self.program = program
        self.store = dict()

    def __getitem__(self, name):
        return self.store[name]

    def __setitem__(self, name, value):
        self.program._set_uniform(name, value)
        self.store[name] = value

class Program:
    def __init__(self):
        self.handle = None
        self.uniform = Uniform(self)

    def compile(self, vertex_code, fragment_code):
        self.handle = shaders.compileProgram(
            shaders.compileShader(vertex_code, GL_VERTEX_SHADER),
            shaders.compileShader(fragment_code, GL_FRAGMENT_SHADER)
        )

    def use(self):
        """
        Configures the program to be used for rendering.
        """
        glUseProgram(self.handle)

    def _set_uniform(self, var, value):

        loc = glGetUniformLocation(self.handle, str(var))

        if loc == -1:
            raise Exception("key not found")

        if isinstance(value, (int, float)):
            glUniform1f(loc, value)
            return

        if isinstance(value, glm.vec2):
            glUniform2fv(loc, 1, glm.value_ptr(value))
            return

        if isinstance(value, glm.vec3):
            glUniform3fv(loc, 1, glm.value_ptr(value))
            return

        if isinstance(value, glm.vec4):
            glUniform4fv(loc, 1, glm.value_ptr(value))
            return

        if isinstance(value, glm.mat3):
            glUniformMatrix3fv(loc, 1, GL_FALSE, glm.value_ptr(value))
            return

        if isinstance(value, glm.mat4):
            glUniformMatrix4fv(loc, 1, GL_FALSE, glm.value_ptr(value))
            return
        
        raise Exception("unknown type")
