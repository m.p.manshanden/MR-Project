import array
import OpenGL.GL as gl

class Ebo:
    def __init__(self):
        self.handle = gl.glGenBuffers(1)
        self.count = 0

    def bind(self):
        """
        Binds this instance to the element buffer taget.
        """
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.handle)

    def buffer_data(self, data, size=None):
        """
        Takes a string of bytes or a list and pushes it to the buffer.
        """
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, self.handle)

        if isinstance(data, list):
            data = array.array('b', data).tostring()
            
        size = len(data) if size is None else size
        gl.glBufferData(gl.GL_ELEMENT_ARRAY_BUFFER, size, data, gl.GL_STATIC_DRAW)

        # each index is 4 bytes
        self.count = int(size / 4)

    def draw(self):
        gl.glDrawElements(gl.GL_TRIANGLES, self.count, gl.GL_UNSIGNED_INT, None)
