import OpenGL.GL as gl

import graphics.vbo
import graphics.ebo
import graphics.fbo

class Vao:
    def __init__(self):
        self.handle = gl.glGenVertexArrays(1)
        self.vbos = []
        self.ebo: 'graphics.ebo.Ebo'
        self.ebo = None

    def bind(self):
        """
        Binds the vertex array, which enables bindings for all added 
        VAOs and optionally a EBO.
        """
        gl.glBindVertexArray(self.handle)

    def _reset(self):
        # delete and recreate vao
        gl.glDeleteVertexArrays(1, [self.handle])
        self.handle = gl.glGenVertexArrays(1)
        
        self.bind()        
        for vbo in self.vbos:
            vbo.bind()

        if self.ebo is not None:
            self.ebo.bind()

        gl.glBindVertexArray(0)        

    def add_vbo(self, vbo: 'graphics.vbo.Vbo'):
        """
        Adds a vertex buffer to the vao structure.
        """
        self.vbos.append(vbo)
        self._reset()        

    def set_ebo(self, ebo: 'graphics.ebo.Ebo'):
        """
        Adds a element buffer to the vao structure.
        """
        self.ebo = ebo
        self._reset()
    
    def draw(self):
        if self.ebo is not None:
            gl.glDrawElements(gl.GL_TRIANGLES, self.ebo.count, gl.GL_UNSIGNED_INT, None)

        if len(self.vbos) == 0:
            return

        count = max(vbo.count for vbo in self.vbos)
        gl.glDrawElements(gl.GL_TRIANGLES, count, gl.GL_UNSIGNED_INT, None)

    def draw_to_fbo(self, fbo: 'graphics.fbo.Fbo'):
        fbo.bind()
        self.draw()
        gl.glBindFramebuffer(gl.GL_FRAMEBUFFER, 0)


