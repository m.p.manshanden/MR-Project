# Place this script in a directory containing "vectors.json" 
# to generate data.txt which can be used for t-SNE input.

import json

f = open("vectors.json", 'r')
vectors = json.load(f)
f.close()

wf = open("data.txt", "w")

for vec in vectors.values():
    vals = vec.values()
    vals = map(str, vals)

    wf.write(" ".join(vals) + "\n")

wf.close()
