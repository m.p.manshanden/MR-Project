﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;

namespace MultimediaRetrieval
{
    using Dict = Dictionary<string, string>;

    public class Dataset : IEnumerable<DataItem>
    {
        private struct QueryResult
        {
            public string[] Meshes { get; set; }
            public float[] Distances { get; set; }
        }

        private static readonly Dict labels;

        static Dataset()
        {
            Console.WriteLine("Reading label data");
            string json = File.ReadAllText("Static/Labels.json");
            labels = JsonSerializer.Deserialize<Dict>(json);
        }

        private readonly Process process;
        private readonly string directory;
        private readonly string[] meshes;
        private readonly HashSet<string> names;

        private float[] distances;

        public DataItem this[string name]
        {
            get
            {
                if (!this.names.Contains(name))
                {
                    throw new KeyNotFoundException();
                }

                return new DataItem(name, Path.Combine(this.directory, name), labels[name]);
            }
        }

        public DataItem this[int index]
        {
            get
            {
                string name = this.meshes[index];

                if (this.distances != null)
                {
                    return new DataItem(name, Path.Combine(this.directory, name), labels[name], this.distances[index]);
                }

                return new DataItem(name, Path.Combine(this.directory, name), labels[name]);
            }
        }

        public int Count => this.meshes.Length;

        private Dataset(Process queryProcess, string directory, string[] meshes)
        {
            this.process = queryProcess;
            this.directory = directory;
            this.meshes = meshes;
            this.names = new HashSet<string>(meshes);
        }

        public Dataset Query(string meshPath, int n = 20)
        {
            this.process.StandardInput.WriteLine(meshPath + " " + n);
            this.process.StandardInput.Flush();

            string data = null;

            this.process.OutputDataReceived += (sender, e) =>
            {
                data = e.Data;
            };

            while (data == null)
            {
                Thread.Sleep(50);
            }

            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            QueryResult result = JsonSerializer.Deserialize<QueryResult>(data, options);

            Dataset subset = this.Subset(result.Meshes);
            subset.distances = result.Distances;

            return subset;
        }

        public Dataset Subset(string[] meshes)
        {
            return new Dataset(this.process, this.directory, meshes);
        }

        public IEnumerator<DataItem> GetEnumerator()
        {
            for (int i = 0; i < this.Count; i++)
            {
                yield return this[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public static bool IsValidDirectory(string directory)
        {
            if (!Directory.Exists(directory))
            {
                return false;
            }

            string metadataFile = Path.Combine(directory, "metadata");

            if (!File.Exists(metadataFile))
            {
                return false;
            }

            string[] meshes = Directory.GetDirectories(directory);

            if (meshes.Length < 3)
            {
                return false;
            }

            for (int i = 0; i < meshes.Length; i++)
            {
                string meshj = meshes[i];
                bool mesh = File.Exists(meshes[i] + "/mesh.off");
                bool silhouette1 = File.Exists(meshes[i] + "/silhouette1.png");
                bool silhouette2 = File.Exists(meshes[i] + "/silhouette2.png");
                bool thumbnail = File.Exists(meshes[i] + "/thumbnail.png");
                bool metadata = File.Exists(meshes[i] + "/metadata");

                if (!mesh || !silhouette1 || !silhouette2 || !thumbnail || !metadata)
                {
                    return false;
                }
            }

            return true;
        }

        public static Dataset LoadFromDirectory(Process queryProcess, string directory)
        {
            if (!IsValidDirectory(directory))
            {
                return null;
            }

            string[] meshes = Directory.GetDirectories(directory).Select(Path.GetFileName).ToArray();

            return new Dataset(queryProcess, directory, meshes);
        }

        public void KillProcess()
        {
            this.process.KillTree();
        }
    }
}
