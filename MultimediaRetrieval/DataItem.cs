﻿using MultimediaRetrieval.Geometry;
using System.Drawing;
using System.IO;

namespace MultimediaRetrieval
{
    public struct DataItem
    {
        public readonly string Name;
        public readonly string Mesh;
        public readonly string Thumbnail;
        public readonly string Label;
        public readonly float Distance;

        public DataItem(string name, string path, string label, float distance = -1)
        {
            this.Name = name;
            this.Mesh = Path.Combine(path, "mesh.off");
            this.Thumbnail = Path.Combine(path, "thumbnail.png");
            this.Label = label;
            this.Distance = distance;
        }

        public Mesh LoadMesh()
        {
            return Geometry.Mesh.LoadFromOffFile(this.Mesh);
        }

        public Image LoadThumbnail()
        {
            return Image.FromFile(this.Thumbnail);
        }
    }
}
