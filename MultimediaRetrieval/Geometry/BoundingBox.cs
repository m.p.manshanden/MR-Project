﻿using OpenTK;
using System.Collections.Generic;

namespace MultimediaRetrieval.Geometry
{
    public class BoundingBox
    {
        private Vector3 min, max;

        public Vector3 Center { get; }

        public BoundingBox(Vector3 min, Vector3 max)
        {
            this.min = min;
            this.max = max;

            this.Center = min + (max - min) * 0.5f;
        }

        public static BoundingBox FromVertices(IList<Vector3> vertices)
        {
            var min = new Vector3(System.Single.MaxValue);
            var max = new Vector3(System.Single.MinValue);

            for (int i = 0; i < vertices.Count; i++)
            {
                max = Vector3.ComponentMax(vertices[i], max);
                min = Vector3.ComponentMin(vertices[i], min);
            }

            return new BoundingBox(min, max);
        }
    }
}
