﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace MultimediaRetrieval.Geometry
{
    public class Mesh
    {
        private readonly List<Vector3> vertices, normals;
        private readonly List<int> triangles;

        public Vector3[] Vertices => this.vertices.ToArray();
        public Vector3[] Normals => this.normals.ToArray();
        public int[] Triangles => this.triangles.ToArray();
        public BoundingBox BoundingBox { get; }
        public Vector3 Center => this.BoundingBox.Center;

        private Mesh()
        {
        }

        private Mesh(List<Vector3> vertices, List<Vector3> normals, List<int> triangles)
        {
            this.vertices = vertices;
            this.normals = normals;
            this.triangles = triangles;

            this.BoundingBox = BoundingBox.FromVertices(vertices);
        }

        public static Mesh LoadFromOffFile(string path)
        {
            var vertices = new List<Vector3>();
            var normals = new List<Vector3>();
            var indices = new List<int>();

            using (var reader = new StreamReader(path))
            {
                ParseToken(reader, "OFF");

                var positions = new List<Vector3>();

                int[] counts = ParseIntegers(reader);

                for (int i = 0; i < counts[0]; i++)
                {
                    Vector3 vector = ParseVector(reader);
                    positions.Add(vector);
                }

                for (int i = 0; i < counts[1]; i++)
                {
                    int[] integers = ParseIntegers(reader);

                    if (integers[0] != 3)
                    {
                        throw new Exception("Faces with more than 3 vertices are not supported.");
                    }

                    int p = integers[1];
                    int q = integers[2];
                    int r = integers[3];

                    Vector3 a = positions[p];
                    Vector3 b = positions[q];
                    Vector3 c = positions[r];

                    Vector3 ab = a - b;
                    Vector3 ac = a - c;
                    Vector3 normal = Vector3.Cross(ac, ab).Normalized();

                    int j = vertices.Count;

                    vertices.AddRange(new[] { a, b, c });
                    normals.AddRange(new[] { normal, normal, normal });
                    indices.AddRange(new[] { j + 0, j + 1, j + 2 });
                }

                ParseEof(reader);
            }

            return new Mesh(vertices, normals, indices);
        }

        private static string ReadLine(StreamReader reader)
        {
            string line;

            do
            {
                line = reader.ReadLine();

                if (line == null)
                {
                    return null;
                }

                line = line.Trim();
            }
            while (line == String.Empty || line[0] == '#');

            return line;
        }

        private static string ParseToken(StreamReader reader, string token)
        {
            string line = ReadLine(reader);

            if (line != token)
            {
                throw new Exception($"Expected {token}, but got {line}.");
            }

            return line;
        }

        private static int[] ParseIntegers(StreamReader reader)
        {
            string[] parts = ReadLine(reader).Split(' ');

            int[] integers = new int[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                bool succes = Int32.TryParse(parts[i], out integers[i]);

                if (!succes)
                {
                    throw new Exception($"Expected an integer, but got {parts[i]}.");
                }
            }

            return integers;
        }

        private static float[] ParseFloats(StreamReader reader)
        {
            string[] parts = ReadLine(reader).Split(' ');

            float[] floats = new float[parts.Length];

            for (int i = 0; i < parts.Length; i++)
            {
                bool success = Single.TryParse(parts[i], NumberStyles.Any, CultureInfo.InvariantCulture, out floats[i]);

                if (!success)
                {
                    throw new Exception($"Expected a float, but got {parts[i]}.");
                }
            }

            return floats;
        }

        private static Vector3 ParseVector(StreamReader reader)
        {
            float[] floats = ParseFloats(reader);

            if (floats.Length != 3)
            {
                throw new Exception($"Expected 3 floats, but got {floats.Length}.");
            }

            return new Vector3(floats[0], floats[1], floats[2]);
        }

        private static void ParseEof(StreamReader reader)
        {
            string line = ReadLine(reader);

            if (line != null)
            {
                throw new Exception($"Expected eof, but got {line}.");
            }
        }
    }
}
