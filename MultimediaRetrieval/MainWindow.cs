﻿using Microsoft.WindowsAPICodePack.Dialogs;
using MultimediaRetrieval.Controls;
using MultimediaRetrieval.Controls.Dialogs;
using MultimediaRetrieval.Geometry;
using System;
using System.IO;
using System.Windows.Forms;

namespace MultimediaRetrieval
{
    public partial class MainWindow : Form
    {
        private static readonly string defaultDataSetPath = "../../../ProcessedData/Princeton";
        private static readonly string defaultOpenPath = "../../../ProcessedData/";

        private readonly Renderer renderer;
        private Dataset dataset;
        private string meshPath;

        public MainWindow()
        {
            this.InitializeComponent();

            this.renderer = new Renderer()
            {
                Dock = DockStyle.Fill
            };

            this.Controls.Add(this.renderer);

            this.LoadDefaultDataset();
            this.CreateMenu();
        }

        private void LoadDefaultDataset()
        {
            string path = Path.GetFullPath(defaultDataSetPath);

            Console.WriteLine($"Looking for dataset in {path}");

            this.LoadDataset(defaultDataSetPath);

            if (this.dataset != null)
            {
                Console.WriteLine($"Loaded dataset at {path}");
            }
            else
            {
                Console.WriteLine($"No valid dataset found in {path}");
            }
        }

        private void CreateMenu()
        {
            this.Menu = new MainMenu();

            var fileMenu = new MenuItem("File");
            fileMenu.MenuItems.Add("Create Dataset", this.CreateDataset);
            fileMenu.MenuItems.Add("Load Dataset", this.LoadDataset);
            fileMenu.MenuItems.Add("Load Mesh", this.LoadOffMesh);

            this.Menu.MenuItems.Add(fileMenu);

            var rendererMenu = new MenuItem("Renderer");
            rendererMenu.MenuItems.Add("Settings", this.RenderSettings);
            rendererMenu.MenuItems.Add("Reset", this.renderer.ResetToDefault);

            this.Menu.MenuItems.Add(rendererMenu);

            if (this.dataset != null)
            {
                var datasetMenu = new MenuItem("Dataset");
                datasetMenu.MenuItems.Add("Choose Mesh", this.ChooseMesh);
                datasetMenu.MenuItems.Add("Measure\t(k=20)", this.Measure20);
                datasetMenu.MenuItems.Add("Measure\t(k=10)", this.Measure10);
                datasetMenu.MenuItems.Add("Measure\t(k=5)", this.Measure5);

                this.Menu.MenuItems.Add(datasetMenu);
            }

            if (this.dataset != null && this.renderer.Mesh != null)
            {
                var meshMenu = new MenuItem("Mesh");
                meshMenu.MenuItems.Add("Query mesh", this.QueryMesh);

                this.Menu.MenuItems.Add(meshMenu);
            }
        }

        private void LoadDataset(string location)
        {
            if (this.dataset != null)
            {
                this.dataset.KillProcess();
                this.dataset = null;
            }

            if (!Dataset.IsValidDirectory(location))
            {
                return;
            }

            var datasetDialog = new LoadDatasetDialog(location);

            if (datasetDialog.ShowDialog() == DialogResult.OK)
            {
                this.dataset = datasetDialog.Dataset;
                this.CreateMenu();
            }
        }

        private void RenderSettings(object sender, EventArgs e)
        {
            new RenderSettingsDialog(this.renderer).ShowDialog();
        }

        private void QueryMesh(object sender, EventArgs e)
        {
            Dataset result = this.dataset.Query(this.meshPath, 12);

            var meshDialog = new MeshDialog(result)
            {
                Text = "Query results"
            };

            if (meshDialog.ShowDialog() == DialogResult.OK)
            {
                Mesh mesh = meshDialog.Selected.LoadMesh();
                this.meshPath = meshDialog.Selected.Mesh;
                this.renderer.LoadMesh(mesh);
                Console.WriteLine("Loaded mesh " + meshDialog.Selected.Name);
                this.CreateMenu();
            }
        }

        private void LoadDataset(object sender, EventArgs e)
        {
            var folderDialog = new CommonOpenFileDialog()
            {
                Title = "Choose dataset location",
                IsFolderPicker = true,
                InitialDirectory = Path.GetFullPath(defaultOpenPath)
            };

            if (folderDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                this.LoadDataset(folderDialog.FileName);
            }
        }

        private void CreateDataset(object sender, EventArgs e)
        {
            var sourceLocationDialog = new CommonOpenFileDialog()
            {
                Title = "Choose source location",
                InitialDirectory = Path.GetFullPath(defaultOpenPath),
                IsFolderPicker = true
            };

            if (sourceLocationDialog.ShowDialog() != CommonFileDialogResult.Ok)
            {
                return;
            }

            var targetLocationDialog = new CommonOpenFileDialog()
            {
                Title = "Choose target location",
                InitialDirectory = Path.GetFullPath(defaultOpenPath),
                IsFolderPicker = true
            };

            if (targetLocationDialog.ShowDialog() != CommonFileDialogResult.Ok)
            {
                return;
            }

            var datasetDialog = new CreateDatasetDialog(sourceLocationDialog.FileName, targetLocationDialog.FileName);

            if (datasetDialog.ShowDialog() == DialogResult.OK)
            {
                this.LoadDataset(targetLocationDialog.FileName);
            }
        }

        private void LoadOffMesh(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog
            {
                Title = "Browse",
                DefaultExt = "off",
                Filter = "OFF Meshes|*.off",
                CheckFileExists = true,
                CheckPathExists = true,
                Multiselect = false
            };

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                var mesh = Mesh.LoadFromOffFile(fileDialog.FileName);
                this.meshPath = fileDialog.FileName;
                this.renderer.LoadMesh(mesh);
                this.CreateMenu();
            }
        }

        private void ChooseMesh(object sender, EventArgs e)
        {
            var meshDialog = new MeshDialog(this.dataset)
            {
                Text = "Choose mesh"
            };

            if (meshDialog.ShowDialog() == DialogResult.OK)
            {
                Mesh mesh = meshDialog.Selected.LoadMesh();
                this.renderer.LoadMesh(mesh);
                this.meshPath = meshDialog.Selected.Mesh;
                Console.WriteLine("Loaded mesh " + meshDialog.Selected.Name);
                this.CreateMenu();
            }
        }

        private void Measure5(object sender, EventArgs e) { this.Measure(5); }
        private void Measure10(object sender, EventArgs e) { this.Measure(10); }
        private void Measure20(object sender, EventArgs e) { this.Measure(20); }

        private void Measure(int k)
        {
            var measureDialog = new MeasureDatasetDialog(this.dataset, k);

            if (measureDialog.ShowDialog() == DialogResult.OK)
            {
                new MeasureResultsDialog(measureDialog.Results1, measureDialog.Results2, measureDialog.Results3).Show();
            }
        }
    }
}
