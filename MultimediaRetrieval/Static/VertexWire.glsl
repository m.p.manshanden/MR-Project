﻿#version 330 core

uniform mat4 Projection;
uniform mat4 View;

layout(location = 0) in vec3 v_Position;

void main()
{
	gl_Position = Projection * View * vec4(v_Position, 1);
}
