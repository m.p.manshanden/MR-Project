﻿#version 330 core

uniform vec3 light_Color;
uniform vec3 light_Direction;
uniform float light_MinIntensity;

in vec3 f_Position;
in vec3 f_Normal;

out vec4 out_Color;

vec4 lambert(vec3 lightDirection, vec3 lightColor, float minValue)
{
	lightDirection = normalize(lightDirection);
	float nDotL = dot(lightDirection, f_Normal);
	float intensity = abs(nDotL) * (1.0 - minValue) + minValue;
	return vec4(intensity * lightColor, 1);
}

void main()
{
    out_Color = lambert(light_Direction.xyz, light_Color.rgb, light_MinIntensity);
}
