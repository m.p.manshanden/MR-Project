﻿#version 330 core

uniform vec3 light_Color;

out vec4 out_Color;

void main()
{
    out_Color = vec4(light_Color, 1);
}
