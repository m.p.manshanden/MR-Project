﻿#version 330 core

uniform mat4 Projection;
uniform mat4 View;

layout(location = 0) in vec3 v_Position;
layout(location = 1) in vec3 v_Normal;

out vec3 f_Position;
out vec3 f_Normal;

void main()
{
	f_Position = v_Position;
    f_Normal = v_Normal;

	gl_Position = Projection * View * vec4(v_Position, 1);
}
